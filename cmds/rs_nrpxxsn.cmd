require stream
require rs_nrpxxsn,master
require autosave

epicsEnvSet("TOP",     "$(E3_CMD_TOP)/..")
epicsEnvSet("AS_TOP",  "$(E3_CMD_TOP)")
epicsEnvSet("IOCNAME", "TS2-010:Ctrl-IOC-002")
epicsEnvSet("SETTINGS_FILES","settings")

iocshLoad("$(TOP)/envs/TS2.env")
iocshLoad("$(autosave_DIR)/autosave.iocsh")

# epicsEnvSet("COMPANY", "European Spallation Source ERIC")
# epicsEnvSet("ENGINEER","MuYuan Wang <MuYuan.Wang@ess.eu>")

## Add extra startup scripts requirements here
iocshLoad("${rs_nrpxxsn_DIR}/rs_nrpxxsn.iocsh", "P=$(P), R=$(R1), IP=$(IP1), ASYN_PORT=$(R1)")
iocshLoad("${rs_nrpxxsn_DIR}/rs_nrpxxsn.iocsh", "P=$(P), R=$(R2), IP=$(IP2), ASYN_PORT=$(R2)")
iocshLoad("${rs_nrpxxsn_DIR}/rs_nrpxxsn.iocsh", "P=$(P), R=$(R3), IP=$(IP3), ASYN_PORT=$(R3)")
iocshLoad("${rs_nrpxxsn_DIR}/rs_nrpxxsn.iocsh", "P=$(P), R=$(R4), IP=$(IP4), ASYN_PORT=$(R4)")
dbLoadRecords("rs_nrpxxsnCableCal.db","P=$(P), R=$(CAL)")

# Call autosave function
afterInit("fdbrestore("$(AS_TOP)/$(IOCNAME)/save/$(SETTINGS_FILES).sav")")

# Call iocInit to start the IOC
iocInit()
