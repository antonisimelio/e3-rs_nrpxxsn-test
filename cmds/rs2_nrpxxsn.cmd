require stream
require rs_nrpxxsn,master

epicsEnvSet("TOP",   "$(E3_CMD_TOP)/..")
epicsEnvSet("P",    "TS2-010CRM:")
epicsEnvSet("R",    "EMR-RFPM-010:")
epicsEnvSet("IP",   "172.16.110.70")

# epicsEnvSet(“COMPANY”, “European Spallation Source ERIC”)
# epicsEnvSet(“ENGINEER”,“MuYuan Wang <MuYuan.Wang@ess.eu>“)
## Add extra startup scripts requirements here

iocshLoad("${rs_nrpxxsn_DIR}/rs_nrpxxsn.iocsh", "P=$(P), R=$(R), IP=$(IP), ASYN_PORT=$(R)")

# Call iocInit to start the IOC
iocInit()

#dbpf TEST-010CRM:TEST-RFPM-010:TracePowerW.SCAN 8

